package com.wadiya.chetto.engine

import com.wadiya.chetto.engine.boards.Board2D
import com.wadiya.chetto.engine.boards.Coord2D
import com.wadiya.chetto.engine.boards.Coord2D.Companion.getCoordByName
import com.wadiya.chetto.engine.boards.Move
import com.wadiya.chetto.engine.gamemodes.ClassicChess

val typeMap = PieceType.values().map { pieceType ->
    pieceType.char to pieceType
}.toMap()

const val FILES = "abcdefgh"

enum class ParseState { START, FOUND_FROM, FOUND_LETTER, FOUND_FIRST_COORD }

class GameNotation {
    companion object {
        fun parseSanNotation(fullText: String): ClassicChess {
            val board = Board2D()
            val gameMode = ClassicChess(board)
            board.loadDefaultStartingPosition()

            fullText.split(" ").forEachIndexed { i, str ->
                // Strip off move number if present
                var chars = str.split(".").last()
                // Deduce piece color from move number
                val color = if (i % 2 == 0) { 0 } else { 1 }

                when(chars) {
                    "O-O-O", "0-0-0" -> gameMode.applyMove(when(color) {
                        0 -> Move(Coord2D(4, 0), Coord2D(2, 0))
                        else -> Move(Coord2D(4, 7), Coord2D(2, 7))
                    })
                    "O-O", "0-0" -> gameMode.applyMove(when(color) {
                        0 -> Move(Coord2D(4, 0), Coord2D(6, 0))
                        else -> Move(Coord2D(4, 7), Coord2D(6, 7))
                    })
                    else -> {
                        // We can always work out the piece type from the first character
                        val pieceType = typeMap[chars[0]]
                            ?.also { chars = chars.drop(1) }
                            ?: PieceType.PAWN

                        var state = ParseState.START
                        var fromFile: Int? = null
                        var fromRank: Int? = null
                        var fromCoord: Coord2D? = null
                        var toCoord: Coord2D? = null
                        var letter: Char? = null
                        var firstCoord: Coord2D? = null
                        chars.forEach { c ->
                            when(state) {
                                ParseState.START -> {
                                    if (c.isDigit()) {
                                        // This must be the 'from' piece's rank; find the piece
                                        fromRank = c.toString().toInt() - 1
                                        state = ParseState.FOUND_FROM
                                    } else if (FILES.contains(c)) {
                                        letter = c
                                        state = ParseState.FOUND_LETTER
                                    }
                                }
                                ParseState.FOUND_LETTER -> {
                                    if (c.isDigit()) {
                                        // Found whole coord; could be either from or to
                                        firstCoord = getCoordByName(letter!!.toString() + c)
                                        letter = null
                                        state = ParseState.FOUND_FIRST_COORD
                                    } else if (FILES.contains(c)) {
                                        // Letter follows, so first letter must have been 'from' file
                                        fromFile = letter!!.code - 'a'.code
                                        letter = c
                                        state = ParseState.FOUND_FROM
                                    }
                                }
                                ParseState.FOUND_FIRST_COORD -> {
                                    if (FILES.contains(c)) {
                                        // New coord follows, so first one must have been 'from'
                                        fromCoord = firstCoord
                                        firstCoord = null
                                        letter = c
                                    }
                                }
                                ParseState.FOUND_FROM -> {
                                    if (FILES.contains(c)) {
                                        letter = c
                                    } else if (letter != null && c.isDigit()) {
                                        toCoord = getCoordByName(letter!!.toString() + c)
                                        if (fromCoord == null) {
                                            // Now it's time to convert a 'from' file/rank into a full 'from' coord
                                            if (fromFile != null) {
                                                fromCoord = board.findPieces(
                                                    colors = setOf(color), types = setOf(pieceType)
                                                ).first {
                                                    it.x == fromFile
                                                    && gameMode.isValidMove(Move(it, toCoord!!))
                                                }
                                            }
                                            if (fromRank != null) {
                                                fromCoord = board.findPieces(
                                                    colors = setOf(color), types = setOf(pieceType)
                                                ).first {
                                                    it.y == fromRank
                                                    && gameMode.isValidMove(Move(it, toCoord!!))
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (toCoord == null && firstCoord != null) {
                            // Only one coord given; must be 'to'
                            toCoord = firstCoord
                            // Means there should be only one possible 'from' coord
                            fromCoord = board.findPieces(
                                colors = setOf(color), types = setOf(pieceType)
                            ).first {
                                gameMode.isValidMove(Move(it, toCoord!!))
                            }
                        }

                        require(fromCoord != null)
                        require(toCoord != null)
                        require(gameMode.isValidMove(Move(fromCoord!!, toCoord!!)))

                        gameMode.applyMove(Move(fromCoord!!, toCoord!!))
                    }
                }
            }

            return gameMode
        }
    }
}