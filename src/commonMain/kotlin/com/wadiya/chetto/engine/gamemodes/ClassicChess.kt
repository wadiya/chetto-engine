package com.wadiya.chetto.engine.gamemodes

import com.wadiya.chetto.engine.Piece
import com.wadiya.chetto.engine.PieceType
import com.wadiya.chetto.engine.boards.*
import kotlin.jvm.JvmName
import kotlin.math.abs

class ClassicChess(
    // This 'JvmName' stuff is required to avoid clash with 'getBoard' function
    @get:JvmName("getMyBoard") val board: Board<Coord2D>
): GameMode<Coord2D> {

    var whiteCanCastleQueenSide = true
    var whiteCanCastleKingSide = true
    var blackCanCastleQueenSide = true
    var blackCanCastleKingSide = true

    override fun getBoard(): Board<Coord2D> = board

    fun basicPieceMoves(from: Coord2D): MutableSet<Coord2D> {
        val piece = board[from]?.piece ?: return mutableSetOf()
        val color = piece.color
        val type = piece.type

        val forwards = if (color == 0) { MoveDirection.BLACK } else { MoveDirection.WHITE }
        val pawnRow = if (color == 0) { 1 } else { 6 }
        val friendlyColors = setOf(color)

        val validMoves = mutableSetOf<Coord2D>()
        when (type) {
            PieceType.PAWN -> {
                validMoves += board.getValidMoves(
                    from, MoveType.PERPENDICULAR, friendlyColors, direction = forwards,
                    maxDist = if (from.y == pawnRow) { 2 } else { 1 }
                ).filter { board[it]!!.piece == null }
                validMoves += board.getValidMoves(
                    from, MoveType.DIAGONAL, friendlyColors, direction = forwards, maxDist = 1
                ).filter { board[it]!!.piece != null }
            }
            PieceType.KING -> {
                validMoves += board.getValidMoves(
                    from, MoveType.PERPENDICULAR, friendlyColors, maxDist = 1
                ) + board.getValidMoves(
                    from, MoveType.DIAGONAL, friendlyColors, maxDist = 1
                )
            }
            PieceType.QUEEN -> {
                validMoves += board.getValidMoves(
                    from, MoveType.PERPENDICULAR, friendlyColors
                ) + board.getValidMoves(
                    from, MoveType.DIAGONAL, friendlyColors
                )
            }
            PieceType.KNIGHT -> {
                validMoves += board.getValidMoves(from, MoveType.KNIGHT, friendlyColors, maxDist = 1)
            }
            PieceType.BISHOP -> {
                validMoves += board.getValidMoves(from, MoveType.DIAGONAL, friendlyColors)
            }
            PieceType.ROOK -> {
                validMoves += board.getValidMoves(from, MoveType.PERPENDICULAR, friendlyColors)
            }
        }
        return validMoves
    }

    private fun isAttacked(coord: Coord2D, enemyPieces: Set<Coord2D>) =
        enemyPieces.any { enemy ->
            basicPieceMoves(enemy).contains(coord)
        }

    private fun getCastleMove(notMovedYet: Boolean, kingCoord: Coord2D, kingSide: Boolean): Coord2D? {
        val kingTo = Coord2D(kingCoord.x + if (kingSide) { +2 } else { -2 }, kingCoord.y )
        try {
            require(notMovedYet)
            val rookCoord = Coord2D(if (kingSide) { 7 } else { 0 }, kingCoord.y)
            val enemyPieces = board.findPieces(colors = setOf(1 - board[kingCoord]!!.piece!!.color))
            // Space between king and rook must be empty
            require(
                if (kingSide) {
                    kingCoord.x + 1 until rookCoord.x
                } else {
                    kingCoord.x - 1 downTo rookCoord.x + 1
                }.all { x ->
                    board[Coord2D(x, kingCoord.y)]!!.piece == null
                }
            )
            // King start and end locations, and the space in-between, must not be under attack
            require(
                if (kingSide) {
                    kingCoord.x..kingTo.x
                } else {
                    kingCoord.x downTo kingTo.x
                }.all { x ->
                    !isAttacked(Coord2D(x, kingCoord.y), enemyPieces)
                }
            )
        } catch (e: IllegalArgumentException) {
            return null
        }
        return kingTo
    }

    override fun getValidMoves(from: Coord2D): Set<Coord2D> {
        val piece = board[from]?.piece ?: return setOf()
        val color = piece.color
        val type = piece.type

        // Basic piece movement
        var validMoves =  basicPieceMoves(from)

        // Castling (allow king to move 2 steps)
        if (type == PieceType.KING) {
            when(color) {
                0 -> {
                    getCastleMove(whiteCanCastleKingSide, from, true)?.let { validMoves.add(it) }
                    getCastleMove(whiteCanCastleQueenSide, from, false)?.let { validMoves.add(it) }
                }
                1 -> {
                    getCastleMove(blackCanCastleKingSide, from, true)?.let { validMoves.add(it) }
                    getCastleMove(blackCanCastleQueenSide, from, false)?.let { validMoves.add(it) }
                }
            }
        }

        // En-passant
        if (type == PieceType.PAWN)
            listOf(board[Coord2D(from.x - 1, from.y)], board[Coord2D(from.x + 1, from.y)])
                .firstOrNull { square ->
                    square?.piece == Piece(1 - color, PieceType.PAWN)
                    && board.getMoveHistory().last().let {
                        it.to == square.coord && abs(it.to.y - it.from.y) == 2
                    }
                }
                ?.let { square ->
                    validMoves.add(
                        Coord2D(square.coord.x, square.coord.y + if (square.piece!!.color == 0) { -1 } else { 1 })
                    )
                }

        // Prevent moving into check
        val enemyPieces = board.findPieces(colors = setOf(1 - color))
        validMoves = validMoves.filter { coord ->
            val tmpBoard = board.clone()
            val tmpGameMode = ClassicChess(tmpBoard)
            tmpBoard.applyMove(Move(from, coord))
            val ourKing = tmpBoard.findPieces(colors = setOf(color), types = setOf(PieceType.KING)).first()
            !tmpGameMode.isAttacked(ourKing, enemyPieces)
        }.toMutableSet()

        return validMoves
    }

    override fun applyMove(move: Move<Coord2D>) {
        val movingPiece = board[move.from]!!.piece!!

        // Castling (add in the rook movement)
        val dx = move.to.x - move.from.x
        if (movingPiece.type == PieceType.KING && abs(dx) == 2) {
            board.applyMove(
                if (dx == 2) { // King side
                    Move(Coord2D(7, move.from.y), Coord2D(5, move.from.y))
                } else { // Queen side
                    Move(Coord2D(0, move.from.y), Coord2D(3, move.from.y))
                },
                addToHistory = false
            )
        }

        // Once king or rook move, castling is no longer possible with those pieces
        if (movingPiece.type == PieceType.KING) {
            when (movingPiece.color) {
                0 -> {
                    whiteCanCastleKingSide = false
                    whiteCanCastleQueenSide = false
                }
                1 -> {
                    blackCanCastleKingSide = false
                    blackCanCastleQueenSide = false
                }
            }
        } else if(movingPiece.type == PieceType.ROOK) {
            when(movingPiece.color) {
                0 -> {
                    when(move.from.x) {
                        7 -> whiteCanCastleKingSide = false
                        0 -> whiteCanCastleQueenSide = false
                    }
                }
                1 -> {
                    when(move.from.x) {
                        7 -> blackCanCastleKingSide = false
                        0 -> blackCanCastleQueenSide = false
                    }
                }
            }
        }

        // En-passant
        if (movingPiece.type == PieceType.PAWN)
            listOf(board[Coord2D(move.from.x - 1, move.from.y)], board[Coord2D(move.from.x + 1, move.from.y)])
                .firstOrNull { square ->
                    square?.piece == Piece(1 - movingPiece.color, PieceType.PAWN)
                    && board.getMoveHistory().last().let {
                        it.to == square.coord && abs(it.to.y - it.from.y) == 2
                    }
                }
                ?.let { square ->
                    square.piece = null
                    // TODO: 28/11/2021 Not very neat to just '= null'; the board class should handle piece destruction 
                    // TODO: 28/11/2021 Should also avoid this duplicate code from `getValidMoves` en-passant logic
                }

        // Pawn promotion
        if (
            (movingPiece == Piece(0, PieceType.PAWN) && move.to.y == 7)
            || (movingPiece == Piece(1, PieceType.PAWN) && move.to.y == 0)
        ){
            require(move.promoteTo!!.type != PieceType.PAWN)
        } else {
            require(move.promoteTo == null)
        }

        board.applyMove(move)
    }
}