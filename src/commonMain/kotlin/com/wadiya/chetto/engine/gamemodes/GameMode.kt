package com.wadiya.chetto.engine.gamemodes

import com.wadiya.chetto.engine.boards.Board
import com.wadiya.chetto.engine.boards.Move

interface GameMode<Coord> {
    fun getBoard(): Board<Coord>
    fun getValidMoves(from: Coord): Set<Coord>

    fun isValidMove(move: Move<Coord>): Boolean {
        val validMoves = getValidMoves(move.from)
        return validMoves.contains(move.to)
    }

    fun applyMove(move: Move<Coord>) {
        getBoard().applyMove(move)
    }
}