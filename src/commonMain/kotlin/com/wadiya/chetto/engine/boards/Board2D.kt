package com.wadiya.chetto.engine.boards

import com.wadiya.chetto.engine.Piece
import com.wadiya.chetto.engine.PieceType

data class Coord2D(val x: Int, val y: Int) {
    operator fun plus(other: Coord2D) = Coord2D(x + other.x, y+other.y)
    operator fun times(other: Int) = Coord2D(x * other, y * other)

    fun getCoordName(coord: Coord2D): String = ('a' + coord.x) + (coord.y + 1).toString()
    companion object {
        fun getCoordByName(name: String) = Coord2D(
            name[0].code - 'a'.code,
            name[1].toString().toInt() - 1
        )
    }
}

typealias Square2D = Square<Coord2D>

val PERP_DIRECTIONS = mapOf(
    MoveDirection.QUEEN to setOf(Coord2D(-1, 0)),
    MoveDirection.KING to setOf(Coord2D(1, 0)),
    MoveDirection.WHITE to setOf(Coord2D(0, -1)),
    MoveDirection.BLACK to setOf(Coord2D(0, 1))
)

val DIAG_DIRECTIONS = mapOf(
    MoveDirection.QUEEN to setOf(Coord2D(-1, -1), Coord2D(-1, 1)),
    MoveDirection.KING to setOf(Coord2D(1, -1), Coord2D(1, 1)),
    MoveDirection.WHITE to setOf(Coord2D(-1, -1), Coord2D(1, -1)),
    MoveDirection.BLACK to setOf(Coord2D(-1, 1), Coord2D(1, 1)),
)

val KNIGHT_DIRECTIONS = mapOf(
    MoveDirection.QUEEN to setOf(Coord2D(-2, -1), Coord2D(-2, 1)),
    MoveDirection.KING to setOf(Coord2D(2, -1), Coord2D(2, 1)),
    MoveDirection.WHITE to setOf(Coord2D(-1, -2), Coord2D(1, -2)),
    MoveDirection.BLACK to setOf(Coord2D(-1, 2), Coord2D(1, 2)),
)

class Board2D: Board<Coord2D> {
    val WIDTH = 8
    val HEIGHT = 8

    val squaresDict = mutableMapOf(
        *(0 until WIDTH).flatMap { x ->
            (0 until HEIGHT).map { y ->
                val coord = Coord2D(x, y)
                val square = Square2D(coord, (x+y) % 2 != 0)
                coord to square
            }
        }.toTypedArray()
    )

    val pastMoves: MutableList<Move<Coord2D>> = mutableListOf()

    override operator fun get(coord: Coord2D): Square2D? = squaresDict[coord]
    override operator fun set(coord: Coord2D, square: Square2D?) {
        square?.let { squaresDict[coord] = it } ?: run { squaresDict.remove(coord) }
    }

    override fun clone(): Board<Coord2D> {
        val newBoard = Board2D()
        newBoard.getSquares().forEach { square ->
            square.piece = this[square.coord]!!.piece
        }
        newBoard.pastMoves.addAll(pastMoves)
        return newBoard
    }


    override fun loadDefaultStartingPosition() {
        pastMoves.clear()

        // Pawn rows
        listOf(Pair(1, 0), Pair(6, 1)).forEach { (row, color) ->
            (0..7).forEach { i ->
                this[Coord2D(i, row)]!!.piece = Piece(color, PieceType.PAWN)
            }
        }

        // Other pieces
        listOf(Pair(0, 0), Pair(7, 1)).forEach { (row, color) ->
            (0..7).forEach { i ->
                this[Coord2D(i, row)]!!.piece = when(i) {
                    0, 7 -> Piece(color, PieceType.ROOK)
                    1, 6 -> Piece(color, PieceType.KNIGHT)
                    2, 5 -> Piece(color, PieceType.BISHOP)
                    3 -> Piece(color, PieceType.QUEEN)
                    4 -> Piece(color, PieceType.KING)
                    else -> { null }
                }
            }
        }
    }

    override fun getSquares(): List<Square2D> = squaresDict.values.toList()

    override fun getValidMoves(
        from: Coord2D,
        type: MoveType,
        friendlyColors: Set<Int>,
        direction: MoveDirection?,
        canJump: Boolean,
        minDist: Int,
        maxDist: Int?
    ): Set<Coord2D> {
        val DIRECTIONS = when(type) {
            MoveType.PERPENDICULAR -> PERP_DIRECTIONS
            MoveType.DIAGONAL -> DIAG_DIRECTIONS
            MoveType.KNIGHT -> KNIGHT_DIRECTIONS
        }
        val dirs = direction?.let { DIRECTIONS[it]!! } ?: DIRECTIONS.values.fold(setOf()) { a, b -> a + b }

        return dirs.flatMap { dir ->
            val validCoords = mutableSetOf<Coord2D>()
            for (dist in (minDist..(maxDist ?: Int.MAX_VALUE))) {
                val coord = from + dir * dist
                if (coord.x < 0 || coord.x >= WIDTH || coord.y < 0 || coord.y >= HEIGHT) { break }
                val piece = this[coord]!!.piece
                if (piece == null || !friendlyColors.contains(piece.color)) {
                    validCoords.add(coord)
                }
                if (piece != null && !canJump) {
                    break
                }
            }
            validCoords
        }.toSet()
    }

    override fun applyMove(move: Move<Coord2D>, addToHistory: Boolean) {
        super.applyMove(move, addToHistory)
        if (addToHistory) pastMoves.add(move)
    }

    override fun getMoveHistory(): List<Move<Coord2D>> = pastMoves
}