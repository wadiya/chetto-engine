package com.wadiya.chetto.engine.boards

import com.wadiya.chetto.engine.Piece
import com.wadiya.chetto.engine.PieceType

enum class MoveType {
    PERPENDICULAR, DIAGONAL, KNIGHT
}

enum class MoveDirection {
    QUEEN, KING, WHITE, BLACK
}

enum class SpecialMove {
    CHECK, CHECKMATE, ENPASSANT, CASTLE, PROMOTION
}

data class Square<Coord>(
    val coord: Coord, // Coordinates
    val white: Boolean, // Square color
    var piece: Piece? = null
) {
    override fun hashCode() = coord.hashCode()
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false
        other as Square<*>
        if (coord != other.coord) return false
        return true
    }
}

data class Move<Coord>(
    val from: Coord,
    val to: Coord,
    val promoteTo: Piece? = null,
    // Optional move info for verification purposes (not yet used)
    val piece: Piece? = null,
    val captures: Boolean? = null,
    val capturedPiece: Piece? = null,
    val specialMove: SpecialMove? = null
)

interface Board<Coord> {
    operator fun get(coord: Coord): Square<Coord>?
    operator fun set(coord: Coord, square: Square<Coord>?)
    fun clone(): Board<Coord>

    fun loadDefaultStartingPosition()
    fun getSquares(): List<Square<Coord>>

    fun applyMove(move: Move<Coord>, addToHistory: Boolean = true) {
        try {
            val fromSquare = this[move.from]!!
            val toSquare = this[move.to]!!
            val fromPiece = fromSquare.piece!!

            toSquare.piece = move.promoteTo ?: fromPiece
            fromSquare.piece = null
        } catch (e: NullPointerException) {
            throw NullPointerException("Tried to apply a move on absent piece and/or square(s)")
        }
    }
    
    fun getMoveHistory(): List<Move<Coord>>

    fun getValidMoves(
        from: Coord,
        type: MoveType,
        friendlyColors: Set<Int>, // Set of colors which cannot be eaten
        direction: MoveDirection? = null,
        canJump: Boolean = false,
        minDist: Int = 1,
        maxDist: Int? = null
    ): Set<Coord>

    fun findPieces(colors: Set<Int>? = null, types: Set<PieceType>? = null): Set<Coord> {
        val coords = mutableSetOf<Coord>()
        getSquares().forEach { square ->
            val piece = square.piece ?: return@forEach
            if (
                (colors?.contains(piece.color) != false)
                && (types?.contains(piece.type) != false)
            ) {
                coords.add(square.coord)
            }
        }
        return coords
    }
}