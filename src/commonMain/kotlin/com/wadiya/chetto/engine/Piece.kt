package com.wadiya.chetto.engine

enum class PieceType(val char: Char, val string: String) {
    KING('K', "King"),
    QUEEN('Q', "Queen"),
    ROOK('R', "Rook"),
    BISHOP('B', "Bishop"),
    KNIGHT('N', "Knight"),
    PAWN('P', "Pawn"),
}

data class Piece (val color: Int, val type: PieceType)
