import com.wadiya.chetto.engine.GameNotation
import com.wadiya.chetto.engine.Piece
import com.wadiya.chetto.engine.PieceType
import com.wadiya.chetto.engine.boards.Coord2D
import kotlin.test.Test
import kotlin.test.assertEquals

val gameSanNotation = "1.e4 e5 2.Nf3 Nf6 3.Nxe5 d6 4.Nf3 Nxe4 5.d4 d5 6.Bd3 Bd6 7.O-O O-O 8.c4 c6 9.Nc3 Nxc3 10.bxc3 Bg4 11.cxd5 cxd5 12.Rb1 Nd7 13.h3 Bh5 14.Rb5 Nb6 15.c4 Bxf3 16.Qxf3 dxc4 17.Bc2 Rb8 18.a4 a6 19.Bg5 Qc7 20.Bxh7+ Kxh7 21.Qh5+ Kg8 22.Bf6 Bh2+ 23.Kh1 Qd6 24.Bxg7 Kxg7 25.Rg5+ Kf6 26.Re1 Qe6 27.Rxe6+ fxe6 28.Rg6+ Ke7 29.Rg7+"
val gameResult = mapOf(
    Coord2D(7, 0) to Piece(0, PieceType.KING),

    Coord2D(5, 1) to Piece(0, PieceType.PAWN),
    Coord2D(6, 1) to Piece(0, PieceType.PAWN),
    Coord2D(7, 1) to Piece(1, PieceType.BISHOP),

    Coord2D(7, 2) to Piece(0, PieceType.PAWN),

    Coord2D(0, 3) to Piece(0, PieceType.PAWN),
    Coord2D(2, 3) to Piece(1, PieceType.PAWN),
    Coord2D(3, 3) to Piece(0, PieceType.PAWN),

    Coord2D(7, 4) to Piece(0, PieceType.QUEEN),

    Coord2D(0, 5) to Piece(1, PieceType.PAWN),
    Coord2D(1, 5) to Piece(1, PieceType.KNIGHT),
    Coord2D(4, 5) to Piece(1, PieceType.PAWN),

    Coord2D(1, 6) to Piece(1, PieceType.PAWN),
    Coord2D(4, 6) to Piece(1, PieceType.KING),
    Coord2D(6, 6) to Piece(0, PieceType.ROOK),

    Coord2D(1, 7) to Piece(1, PieceType.ROOK),
    Coord2D(5, 7) to Piece(1, PieceType.ROOK),
)

class GameNotationTest {
    @Test
    fun parseTestGame() {
        val gameMode = GameNotation.parseSanNotation(gameSanNotation)
        gameMode.board.getSquares().forEach{ square ->
            assertEquals(square.piece, gameResult[square.coord])
        }
    }
}