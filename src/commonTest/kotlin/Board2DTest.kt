import com.wadiya.chetto.engine.boards.*
import com.wadiya.chetto.engine.Piece
import com.wadiya.chetto.engine.PieceType
import kotlin.test.*

class Board2DTest {

    val board = Board2D()

    init {
        board[Coord2D(4, 2)]!!.piece = Piece(0, PieceType.PAWN)
    }

    @Test
    fun rookMove() {
        val moves = board.getValidMoves(
            Coord2D(0, 2),
            MoveType.PERPENDICULAR,
            setOf(0),
            MoveDirection.KING
        )
        assertEquals(
            moves,
            setOf(Coord2D(1, 2), Coord2D(2, 2), Coord2D(3, 2))
        )
    }

    @Test
    fun rookMoveEat() {
        val moves = board.getValidMoves(
            Coord2D(0, 2),
            MoveType.PERPENDICULAR,
            setOf(1),
            MoveDirection.KING
        )
        assertEquals(
            moves,
            setOf(Coord2D(1, 2), Coord2D(2, 2), Coord2D(3, 2), Coord2D(4, 2))
        )
    }

    @Test
    fun bishopCanEat() {
        val moves = board.getValidMoves(
            Coord2D(2, 0),
            MoveType.DIAGONAL,
            setOf(1),
        )
        assertTrue(
            moves.contains(Coord2D(4, 2))
        )
    }

    @Test
    fun bishopCannotEat() {
        val moves = board.getValidMoves(
            Coord2D(2, 1),
            MoveType.DIAGONAL,
            setOf(1),
        )
        assertFalse(
            moves.contains(Coord2D(4, 2))
        )
    }

    @Test
    fun knightCanEat() {
        val moves = board.getValidMoves(
            Coord2D(5, 4),
            MoveType.KNIGHT,
            setOf(1),
        )
        assertTrue(
            moves.contains(Coord2D(4, 2))
        )
    }

    @Test
    fun knightCannotEat() {
        val moves = board.getValidMoves(
            Coord2D(5, 5),
            MoveType.KNIGHT,
            setOf(1),
        )
        assertFalse(
            moves.contains(Coord2D(4, 2))
        )
    }
}